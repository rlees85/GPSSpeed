# GPS Speed

A simple GPS tracking application that does NOT depend on Internet connectivity.

This project was never meant to be the flashiest or even the best GPS tracker
ever. It was developed just to serve a purpose for me - primarily for skiing in
foreign countries (data = expensive). I then uploaded to Gitlab incase it is of
use to others.

**Current tried and tested devices:**
 - Fairphone 5 using /e/ OS
 - Sony Xperia Z (YUGA) using CM11, CM12, CM12.1
 - LG Optimus 2X (P990) using CM11

**License**: *You may use the source for this as you wish*. I don't think
anything within this project is subject to any other license.
