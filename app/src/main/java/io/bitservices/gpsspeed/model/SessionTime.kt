package io.bitservices.gpsspeed.model

class SessionTime {
    var timeMoving: Long = 0L
        private set

    var timeMovingUp: Long = 0L
        private set

    var timeMovingDown: Long = 0L
        private set

    var timeMovingUnknown: Long = 0L
        private set

    var timeStopped: Long = 0L
        private set

    private var lastUpdate: Long? = null

    private var lastSentTimeMoving: Long? = null
    private var lastSentTimeMovingUp: Long? = null
    private var lastSentTimeMovingDown: Long? = null
    private var lastSentTimeStopped: Long? = null

    fun add(isMoving: Boolean, direction: Int) {
        val lastUpdateSnapshot: Long? = lastUpdate

        if (lastUpdateSnapshot != null) {
            val timeDifference: Long = System.currentTimeMillis() - lastUpdateSnapshot

            // We have had an update before!
            if (isMoving) {
                // We are moving
                timeMoving += timeDifference

                if (direction == SessionAltitude.ALTITUDE_DIRECTION_UP) {
                    timeMovingUp += timeDifference + timeMovingUnknown
                    timeMovingUnknown = 0L
                } else if (direction == SessionAltitude.ALTITUDE_DIRECTION_DOWN) {
                    timeMovingDown += timeDifference + timeMovingUnknown
                    timeMovingUnknown = 0L
                } else {
                    timeMovingUnknown += timeDifference
                }
            } else {
                // We are not moving
                timeStopped += timeDifference
            }
        }

        lastUpdate = System.currentTimeMillis()
    }

    fun restore(timeMoving: Long, timeMovingUp: Long, timeMovingDown: Long, timeMovingUnknown: Long, timeStopped: Long) {
        this.timeMoving = timeMoving
        this.timeMovingUp = timeMovingUp
        this.timeMovingDown = timeMovingDown
        this.timeMovingUnknown = timeMovingUnknown
        this.timeStopped = timeStopped
    }

    fun shouldSend(): Boolean {
        val result: Boolean = if (lastSentTimeMoving == null || lastSentTimeMovingUp == null || lastSentTimeMovingDown == null || lastSentTimeStopped == null) {
            true
        } else {
            lastSentTimeMoving != timeMoving ||
                    lastSentTimeMovingUp != timeMovingUp ||
                    lastSentTimeMovingDown != timeMovingDown ||
                    lastSentTimeStopped != timeStopped
        }

        lastSentTimeMoving = timeMoving
        lastSentTimeMovingUp = timeMovingUp
        lastSentTimeMovingDown = timeMovingDown
        lastSentTimeStopped = timeStopped
        return result
    }
}