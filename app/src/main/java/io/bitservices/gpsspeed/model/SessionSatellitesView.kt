package io.bitservices.gpsspeed.model

import io.bitservices.gpsspeed.data.satellites.SatellitesViewRecord
import java.time.Instant

class SessionSatellitesView {
    private var satellitesView: MutableMap<String, SatellitesViewRecord> = mutableMapOf()
    private var lastSent: Instant = Instant.MIN

    override fun toString(): String {
        val result: StringBuilder = StringBuilder()
        for (key in satellitesView.keys.sorted()) {
            // Check the record is not too old to be used.
            if (satellitesView[key]!!.timestamp.isAfter(Instant.now().minusSeconds(SATELLITES_VIEW_EXPIRY_SECONDS))) {
                // Append to returned string
                result.append(key + " (" + satellitesView[key]!!.satellites.toString() + ") ")
            }
        }

        return if (result.isEmpty()) {
            "none"
        } else {
            result.toString().trim()
        }
    }

    fun add(system: String, signal: String, satellites: Int) {
        val key: String = "$system:$signal"
        satellitesView[key] = SatellitesViewRecord(satellites)
    }

    fun shouldSend(): Boolean {
        val now: Instant = Instant.now()
        if (now.minusSeconds(SATELLITES_VIEW_EXPIRY_SECONDS).isAfter(lastSent)) {
            lastSent = now
            return true
        } else {
            return false
        }
    }

    companion object {
        const val SATELLITES_VIEW_EXPIRY_SECONDS: Long = 5L
    }
}