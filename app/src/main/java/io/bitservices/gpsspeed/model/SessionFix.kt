package io.bitservices.gpsspeed.model

import kotlin.math.max

class SessionFix {
    var accuracyHorizontal: Float = Float.MAX_VALUE
        get() {
            return max(field, accuracyPositional)
        }

    var accuracyPositional: Float = Float.MAX_VALUE

    var accuracyVertical: Float = Float.MAX_VALUE
        get() {
            return max(field, accuracyPositional)
        }

    var fixed: Boolean = false
        private set

    var satellites: Int = 0

    private var ggaFixed: Boolean = false
    private var gsaFixed: Boolean = false

    private var lastSentFixed: Boolean? = null
    private var lastSentSatellites: Int? = null

    fun setGGA(gga: Boolean): Boolean {
        // return if or not the fix status has changed.

        ggaFixed = gga
        return updateFixStatus()
    }

    fun setGSA(gsa: Boolean): Boolean {
        // return if or not the fix status has changed.

        gsaFixed = gsa
        return updateFixStatus()
    }

    fun isAccurateHorizontal(): Boolean {
        return fixed && (accuracyHorizontal <= MAX_HORIZONTAL_DOP)
    }

    fun isAccurateVertical(): Boolean {
        return fixed && (accuracyVertical <= MAX_VERTICAL_DOP)
    }

    fun shouldSend(): Boolean {
        val result: Boolean = if (lastSentFixed == null || lastSentSatellites == null) {
            true
        } else {
            lastSentFixed != fixed ||
            lastSentSatellites != satellites
        }

        lastSentFixed = fixed
        lastSentSatellites = satellites
        return result
    }

    private fun updateFixStatus(): Boolean {
        val isNowFixed: Boolean = ggaFixed && gsaFixed
        val fixStatusChanged: Boolean = fixed != isNowFixed

        fixed = isNowFixed
        return fixStatusChanged
    }

    companion object {
        private const val MAX_HORIZONTAL_DOP: Float = 2f
        private const val MAX_VERTICAL_DOP: Float = 1.5f
    }
}