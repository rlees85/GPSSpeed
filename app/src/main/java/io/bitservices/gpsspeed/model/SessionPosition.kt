package io.bitservices.gpsspeed.model

import io.bitservices.gpsspeed.data.Position
import kotlin.math.PI
import kotlin.math.atan2
import kotlin.math.cos
import kotlin.math.pow
import kotlin.math.sin
import kotlin.math.sqrt

class SessionPosition {
    private var distance: Double = 0.0
    private var distanceUp: Double = 0.0
    private var distanceDown: Double = 0.0
    private var distanceUnknown: Double = 0.0

    private var position: Position = Position()

    private var lastSentDistance: Double? = null
    private var lastSentDistanceUp: Double? = null
    private var lastSentDistanceDown: Double? = null

    fun add(position: Position, direction: Int) {
        if (position.valid) {
            if (this.position.valid) {
                val distanceMoved: Double = getDistance(this.position, position)
                distance += distanceMoved

                if (direction == SessionAltitude.ALTITUDE_DIRECTION_UP) {
                    distanceUp += distanceMoved + distanceUnknown
                    distanceUnknown = 0.0
                } else if (direction == SessionAltitude.ALTITUDE_DIRECTION_DOWN) {
                    distanceDown += distanceMoved + distanceUnknown
                    distanceUnknown = 0.0
                } else {
                    distanceUnknown += distanceMoved
                }
            }

            this.position = position
        }
    }
    fun distance(unit: Int = POSITION_DISTANCE_UNIT_KILOMETERS): Double {
        return if (unit == POSITION_DISTANCE_UNIT_MILES) {
            distance * EARTH_RADIUS_MILES
        } else if (unit == POSITION_DISTANCE_UNIT_KILOMETERS) {
            distance * EARTH_RADIUS_KILOMETERS
        } else {
            distance
        }
    }

    fun distanceUp(unit: Int = POSITION_DISTANCE_UNIT_KILOMETERS): Double {
        return if (unit == POSITION_DISTANCE_UNIT_MILES) {
            distanceUp * EARTH_RADIUS_MILES
        } else if (unit == POSITION_DISTANCE_UNIT_KILOMETERS) {
            distanceUp * EARTH_RADIUS_KILOMETERS
        } else {
            distanceUp
        }
    }

    fun distanceDown(unit: Int = POSITION_DISTANCE_UNIT_KILOMETERS): Double {
        return if (unit == POSITION_DISTANCE_UNIT_MILES) {
            distanceDown * EARTH_RADIUS_MILES
        } else if (unit == POSITION_DISTANCE_UNIT_KILOMETERS) {
            distanceDown * EARTH_RADIUS_KILOMETERS
        } else {
            distanceDown
        }
    }

    fun distanceUnknown(unit: Int = POSITION_DISTANCE_UNIT_RAW): Double {
        return if (unit == POSITION_DISTANCE_UNIT_MILES) {
            distanceUnknown * EARTH_RADIUS_MILES
        } else if (unit == POSITION_DISTANCE_UNIT_KILOMETERS) {
            distanceUnknown * EARTH_RADIUS_KILOMETERS
        } else {
            distanceUnknown
        }
    }

    fun restore(distance: Double, distanceUp: Double, distanceDown: Double, distanceUnknown: Double) {
        this.distance = distance
        this.distanceUp = distanceUp
        this.distanceDown = distanceDown
        this.distanceUnknown = distanceUnknown
    }

    fun shouldSend(): Boolean {
        val result: Boolean = if (lastSentDistance == null || lastSentDistanceUp == null || lastSentDistanceDown == null) {
            true
        } else {
            lastSentDistance != distance ||
                    lastSentDistanceUp != distanceUp ||
                    lastSentDistanceDown != distanceDown
        }

        lastSentDistance = distance
        lastSentDistanceUp = distanceUp
        lastSentDistanceDown = distanceDown
        return result
    }

    private fun getDistance(start: Position, end: Position): Double {
        val startLatitudeRad: Double = start.latitude * (PI / 180)
        val startLongitudeRad: Double = start.longitude * (PI / 180)
        val endLatitudeRad: Double = end.latitude * (PI / 180)
        val endLongitudeRad: Double = end.longitude * (PI / 180)

        val differenceLatitudeRad = endLatitudeRad - startLatitudeRad
        val differenceLongitudeRad = endLongitudeRad - startLongitudeRad

        val workingValue: Double = sin(differenceLatitudeRad / 2).pow(2) + cos(startLatitudeRad) * cos(endLatitudeRad) * sin(differenceLongitudeRad /2).pow(2)
        return 2 * atan2(sqrt(workingValue), sqrt(1 - workingValue))
    }

    companion object {
        const val POSITION_DISTANCE_UNIT_RAW: Int = 0
        const val POSITION_DISTANCE_UNIT_KILOMETERS: Int = 1
        const val POSITION_DISTANCE_UNIT_MILES: Int = 2

        private const val EARTH_RADIUS_KILOMETERS: Double = 6367.5
        private const val EARTH_RADIUS_MILES: Double = 3956.5
    }
}