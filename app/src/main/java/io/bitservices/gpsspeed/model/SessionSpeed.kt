package io.bitservices.gpsspeed.model

import io.bitservices.gpsspeed.data.Speed

class SessionSpeed {
    private val isMovingSpeed: Speed = Speed.fromKph(IS_MOVING_KPH)

    var maxSpeed: Speed = Speed.fromNone()

    var lastSentSpeed: Speed? = null
    var lastSentMaxSpeed: Speed? = null

    var speed: Speed = Speed.fromNone()
        set(value) {
            field = value

            if (value.kph > maxSpeed.kph &&
                value.knots > maxSpeed.knots) {
                maxSpeed = value
            }
        }

    fun isMoving(): Boolean {
        return speed.kph >= isMovingSpeed.kph
    }

    fun shouldSend(): Boolean {
        val result: Boolean = if (lastSentSpeed == null || lastSentMaxSpeed == null) {
            true
        } else {
            lastSentSpeed?.knots != speed.knots ||
                    lastSentSpeed?.kph != speed.kph ||
                    lastSentMaxSpeed?.knots != maxSpeed.knots ||
                    lastSentMaxSpeed?.kph == maxSpeed.kph
        }

        lastSentSpeed = speed
        lastSentMaxSpeed = maxSpeed
        return result
    }

    companion object {
        private const val IS_MOVING_KPH: Float = 2f
    }
}