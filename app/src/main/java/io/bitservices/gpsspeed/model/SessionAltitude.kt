package io.bitservices.gpsspeed.model

class SessionAltitude {

    private var reference: Float? = null

    private var lastSentAltitude: Float? = null
    private var lastSentHighest: Float? = null
    private var lastSentLowest: Float? = null

    var altitude: Float = 0f
        set(value) {
            val referenceSnapshot: Float? = reference

            if (referenceSnapshot == null) {
                reference = value
            } else {
                // Update our direction indicator if sufficient movement from reference point
                if (value >= (referenceSnapshot + ALTITUDE_MIN_MOVEMENT)) {
                    direction = ALTITUDE_DIRECTION_UP
                    reference = value
                } else if (value <= (referenceSnapshot - ALTITUDE_MIN_MOVEMENT)) {
                    direction = ALTITUDE_DIRECTION_DOWN
                    reference = value
                }
            }

            if (initialised) {
                // Check if we made records for lowest or highest altitude
                if (value < lowest) {
                    lowest = value
                } else if (value > highest) {
                    highest = value
                }
            } else {
                highest = value
                lowest = value
                initialised = true
            }

            field = value
        }

    var direction: Int = ALTITUDE_DIRECTION_UNKNOWN
        private set

    var initialised: Boolean = false
        private set

    var highest: Float = 0f
        private set

    var lowest: Float = 0f
        private set

    fun reset() {
        direction = ALTITUDE_DIRECTION_UNKNOWN
    }

    fun restore(highest: Float, lowest: Float) {
        if (highest >= lowest) {
            this.highest = highest
            this.lowest = lowest
            initialised = true
        }
    }

    fun shouldSend(): Boolean {
        if (!initialised) {
            return false
        }

        val result: Boolean = if (lastSentAltitude == null || lastSentHighest == null || lastSentLowest == null) {
            true
        } else {
            lastSentAltitude != altitude ||
            lastSentHighest != highest ||
            lastSentLowest != lowest
        }

        lastSentAltitude = altitude
        lastSentHighest = highest
        lastSentLowest = lowest
        return result
    }

    companion object {
        const val ALTITUDE_DIRECTION_UNKNOWN = -1
        const val ALTITUDE_DIRECTION_DOWN = 0
        const val ALTITUDE_DIRECTION_UP = 1
        private const val ALTITUDE_MIN_MOVEMENT = 2f
    }
}