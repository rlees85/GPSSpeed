package io.bitservices.gpsspeed

import android.content.Intent
import android.content.SharedPreferences
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.github.anastr.speedviewlib.SpeedView
import com.github.anastr.speedviewlib.components.Section
import io.bitservices.gpsspeed.message.MessageAltitude
import io.bitservices.gpsspeed.message.MessageFix
import io.bitservices.gpsspeed.message.MessagePosition
import io.bitservices.gpsspeed.message.MessageSpeed
import io.bitservices.gpsspeed.message.MessageTime
import io.bitservices.gpsspeed.model.SessionAltitude
import io.bitservices.gpsspeed.model.SessionPosition
import io.bitservices.gpsspeed.model.SessionSpeed
import io.bitservices.gpsspeed.model.SessionTime
import java.time.Duration
import kotlin.math.round

class MainActivity : AppCompatActivity() {
    private lateinit var service: Intent

    private lateinit var oAltitude: Observer<MessageAltitude?>
    private lateinit var oFix: Observer<MessageFix?>
    private lateinit var oPosition: Observer<MessagePosition?>
    private lateinit var oSatellitesView: Observer<String?>
    private lateinit var oService: Observer<Boolean>
    private lateinit var oSpeed: Observer<MessageSpeed?>
    private lateinit var oTime: Observer<MessageTime?>

    private lateinit var svSpeed: SpeedView
    private lateinit var svAltitude: SpeedView
    private lateinit var svSatellites: SpeedView

    private lateinit var tvMaxSpeed: TextView
    private lateinit var tvAverageMovingSpeed: TextView
    private lateinit var tvAverageSpeed: TextView
    private lateinit var tvDistance: TextView
    private lateinit var tvTimeMoving: TextView
    private lateinit var tvTimeMovingUp: TextView
    private lateinit var tvTimeMovingDown: TextView
    private lateinit var tvTimeStopped: TextView
    private lateinit var tvAltitudeHighest: TextView
    private lateinit var tvAltitudeLowest: TextView
    private lateinit var tvAltitudeDifference: TextView
    private lateinit var tvSatellitesView: TextView
    private lateinit var tvVersion: TextView

    private var averageSpeedDistance: MessagePosition? = null

    @ColorInt
    private var errorColor: Int = 0
    @ColorInt
    private var gaugeColor: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.setBackgroundDrawable(ColorDrawable(getColor(R.color.primaryDark)))

        // Setup service intent
        service = Intent(this, MainService::class.java)

        // Setup gauge controls (colour bands, etc)
        svSpeed = findViewById<SpeedView>(R.id.svSpeed)
        svAltitude = findViewById<SpeedView>(R.id.svAltitude)
        svSatellites = findViewById<SpeedView>(R.id.svSatellites)

        svSpeed.clearSections()
        svSpeed.addSections(
            Section(0f, 1f, getColor(R.color.gaugeDefault), svSpeed.speedometerWidth)
        )

        svAltitude.clearSections()
        svAltitude.addSections(
            Section(0f, .1667f, getColor(R.color.gaugePurple), svAltitude.speedometerWidth),
            Section(.1667f, .5833f, getColor(R.color.gaugeDefault), svAltitude.speedometerWidth),
            Section(.5833f, .6667f, getColor(R.color.gaugeYellow), svAltitude.speedometerWidth),
            Section(.6667f, 1f, getColor(R.color.gaugeRed), svAltitude.speedometerWidth)
        )
        svAltitude.onPrintTickLabel = { _: Int, tick: Float ->
            if (tick != 0f) {
                round(tick / 1000f).toInt().toString() + "K"
            } else {
                null
            }
        }

        svSatellites.clearSections()
        svSatellites.addSections(
            Section(0f, .08f, getColor(R.color.gaugeRed), svSatellites.speedometerWidth),
            Section(.08f, .24f, getColor(R.color.gaugeDefault), svSatellites.speedometerWidth),
            Section(.24f, 1f, getColor(R.color.gaugeGreen), svSatellites.speedometerWidth)
        )

        // Setup text fields
        tvMaxSpeed = findViewById<TextView>(R.id.tvMaxSpeed)
        tvAverageMovingSpeed = findViewById<TextView>(R.id.tvAverageMovingSpeed)
        tvAverageSpeed = findViewById<TextView>(R.id.tvAverageSpeed)
        tvDistance = findViewById<TextView>(R.id.tvDistance)
        tvTimeMoving = findViewById<TextView>(R.id.tvTimeMoving)
        tvTimeMovingUp = findViewById<TextView>(R.id.tvTimeMovingUp)
        tvTimeMovingDown = findViewById<TextView>(R.id.tvTimeMovingDown)
        tvTimeStopped = findViewById<TextView>(R.id.tvTimeStopped)
        tvAltitudeHighest = findViewById<TextView>(R.id.tvAltitudeHighest)
        tvAltitudeLowest = findViewById<TextView>(R.id.tvAltitudeLowest)
        tvAltitudeDifference = findViewById<TextView>(R.id.tvAltitudeDifference)
        tvSatellitesView = findViewById<TextView>(R.id.tvSatellitesView)

        tvVersion = findViewById<TextView>(R.id.tvVersion)
        tvVersion.text = BuildConfig.VERSION_NAME

        // Setup base colours
        errorColor = getColor(R.color.textColorError)
        gaugeColor = svSpeed.speedTextColor

        // Reset GUI elements
        stopUi(true)
        resetUi()

        // Setup service observers
        setupObservers()
        // Listen for UI events
        startObservers()

        // Set UI to ready
        MainData.ui.postValue(MainData.LIVEDATA_UI_CONNECTED)
    }

    override fun onResume() {
        super.onResume()

        startObservers()
        MainData.ui.postValue(MainData.LIVEDATA_UI_CONNECTED)
    }

    override fun onPause() {
        MainData.ui.postValue(MainData.LIVEDATA_UI_DISCONNECTED)

        stopObservers()
        super.onPause()
    }

    override fun onStop() {
        MainData.ui.postValue(MainData.LIVEDATA_UI_DISCONNECTED)

        stopObservers()
        super.onStop()
    }

    override fun onDestroy() {
        MainData.ui.postValue(MainData.LIVEDATA_UI_DISCONNECTED)

        stopObservers()
        super.onDestroy()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.activity_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.mnuStart -> startForegroundService(service)
            R.id.mnuStop -> stopService(service)
            R.id.mnuReset -> {
                // Reset persisted data
                val sp: SharedPreferences = getSharedPreferences(getString(R.string.shared_prefs_trip_settings), MODE_PRIVATE)
                sp.edit()
                    .putFloat(getString(R.string.shared_prefs_trip_max_speed_kph_setting), 0f)
                    .putFloat(getString(R.string.shared_prefs_trip_max_speed_knots_setting), 0f)
                    .putFloat(getString(R.string.shared_prefs_trip_distance_setting), 0f)
                    .putFloat(getString(R.string.shared_prefs_trip_distance_up_setting), 0f)
                    .putFloat(getString(R.string.shared_prefs_trip_distance_down_setting), 0f)
                    .putFloat(getString(R.string.shared_prefs_trip_distance_unknown_setting), 0f)
                    .putLong(getString(R.string.shared_prefs_trip_time_moving_setting), 0)
                    .putLong(getString(R.string.shared_prefs_trip_time_moving_up_setting), 0)
                    .putLong(getString(R.string.shared_prefs_trip_time_moving_down_setting), 0)
                    .putLong(getString(R.string.shared_prefs_trip_time_moving_unknown_setting), 0)
                    .putLong(getString(R.string.shared_prefs_trip_time_stopped_setting), 0)
                    .putBoolean(getString(R.string.shared_prefs_trip_altitude_initialised_setting), false)
                    .putFloat(getString(R.string.shared_prefs_trip_altitude_highest_setting), 0f)
                    .putFloat(getString(R.string.shared_prefs_trip_altitude_lowest_setting), 0f)
                    .apply()

                // Tell service to reset
                MainData.ui.postValue(MainData.LIVEDATA_UI_RESET)

                // Drop distance cache for average speed
                averageSpeedDistance = null

                // Reset our GUI
                resetUi()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun setupObservers() {
        oAltitude = Observer { newState ->
            if (newState != null) {
                svAltitude.speedTo(newState.current, NEEDLE_SPEED)
                tvAltitudeHighest.text =
                    getString(R.string.label_altitude_highest).format(newState.highest)
                tvAltitudeLowest.text =
                    getString(R.string.label_altitude_lowest).format(newState.lowest)
                tvAltitudeDifference.text =
                    getString(R.string.label_altitude_difference).format(newState.highest - newState.lowest)
            }
        }

        oFix = Observer { newState ->
            if (newState != null) {
                svSatellites.speedTo(newState.inUse.toFloat(), NEEDLE_SPEED)

                if (newState.fixed) {
                    svSatellites.speedTextColor = gaugeColor
                    svSatellites.unitTextColor = gaugeColor
                } else {
                    svSatellites.speedTextColor = errorColor
                    svSatellites.unitTextColor = errorColor
                }
            }
        }

        oPosition = Observer { newState ->
            if (newState != null) {
                averageSpeedDistance = newState
                tvDistance.text = getString(R.string.label_distance).format(
                    newState.total,
                    newState.up,
                    newState.down
                )
            }
        }

        oSatellitesView = Observer { newState ->
            if (newState != null) {
                tvSatellitesView.text = getString(R.string.label_satellites_view).format(newState)
            }
        }

        oService = Observer { newState ->
            if(!newState) {
                stopUi()
            }
        }

        oSpeed = Observer { newState ->
            if (newState != null) {
                svSpeed.speedTo(newState.current)
                tvMaxSpeed.text = getString(R.string.label_max_speed).format(newState.max)
            }
        }

        oTime = Observer { newState ->
            if (newState != null ) {
                val moving: Duration = Duration.ofMillis(newState.moving)
                val movingUp: Duration = Duration.ofMillis(newState.movingUp)
                val movingDown: Duration = Duration.ofMillis(newState.movingDown)
                val stopped: Duration = Duration.ofMillis(newState.stopped)

                tvTimeMoving.text = getString(R.string.label_time_moving).format(
                    moving.toHours(),
                    moving.toMinutesPart(),
                    moving.toSecondsPart()
                )
                tvTimeMovingUp.text = getString(R.string.label_time_moving_up).format(
                    movingUp.toHours(),
                    movingUp.toMinutesPart(),
                    movingUp.toSecondsPart()
                )
                tvTimeMovingDown.text = getString(R.string.label_time_moving_down).format(
                    movingDown.toHours(),
                    movingDown.toMinutesPart(),
                    movingDown.toSecondsPart()
                )
                tvTimeStopped.text = getString(R.string.label_time_stopped).format(
                    stopped.toHours(),
                    stopped.toMinutesPart(),
                    stopped.toSecondsPart()
                )

                setAverageSpeed(averageSpeedDistance, newState)
            }
        }
    }

    private fun setAverageSpeed(distance: MessagePosition?, times: MessageTime) {
        var averageSpeed: Double = 0.0
        var averageMovingSpeed: Double = 0.0
        var averageMovingUpSpeed: Double = 0.0
        var averageMovingDownSpeed: Double = 0.0

        if (distance != null) {
            val timeTotal: Long = times.moving + times.stopped

            if (timeTotal > 0L) {
                averageSpeed = distance.total / (timeTotal / TIME_MILLS_TO_HOUR)
            }

            if (times.moving > 0L) {
                averageMovingSpeed = distance.total / (times.moving / TIME_MILLS_TO_HOUR)
            }

            if (times.movingUp > 0L) {
                averageMovingUpSpeed = distance.up / (times.movingUp / TIME_MILLS_TO_HOUR)
            }

            if (times.movingDown > 0L) {
                averageMovingDownSpeed = distance.down / (times.movingDown / TIME_MILLS_TO_HOUR)
            }
        }

        tvAverageMovingSpeed.text = getString(R.string.label_average_moving_speed).format(averageMovingSpeed, averageMovingUpSpeed, averageMovingDownSpeed)
        tvAverageSpeed.text = getString(R.string.label_average_speed).format(averageSpeed)
    }

    private fun startObservers() {
        // Drop distance cache for average speed
        averageSpeedDistance = null

        // Start observers
        MainData.service.observe(this, oService)
        MainData.serviceAltitude.observe(this, oAltitude)
        MainData.serviceFix.observe(this, oFix)
        MainData.servicePosition.observe(this, oPosition)
        MainData.serviceSatellitesView.observe(this, oSatellitesView)
        MainData.serviceSpeed.observe(this, oSpeed)
        MainData.serviceTime.observe(this, oTime)
    }

    private fun stopObservers() {
        MainData.service.removeObserver(oService)
        MainData.serviceAltitude.removeObserver(oAltitude)
        MainData.serviceFix.removeObserver(oFix)
        MainData.servicePosition.removeObserver(oPosition)
        MainData.serviceSatellitesView.removeObserver(oSatellitesView)
        MainData.serviceSpeed.removeObserver(oSpeed)
        MainData.serviceTime.removeObserver(oTime)
    }

    private fun stopUi(instant: Boolean = false) {
        tvSatellitesView.text = getString(R.string.label_satellites_view_none)

        if (instant) {
            svSpeed.speedTo(0f, 0)
            svAltitude.speedTo(0f, 0)
            svSatellites.speedTo(0f, 0)
        } else {
            svSpeed.speedTo(0f, NEEDLE_SPEED)
            svAltitude.speedTo(0f, NEEDLE_SPEED)
            svSatellites.speedTo(0f, NEEDLE_SPEED)
        }

        svSatellites.speedTextColor = errorColor
        svSatellites.unitTextColor = errorColor
    }

    private fun resetUi() {
        tvMaxSpeed.text = getString(R.string.label_max_speed_none)
        tvAverageMovingSpeed.text = getString(R.string.label_average_moving_speed_none)
        tvAverageSpeed.text = getString(R.string.label_average_speed_none)
        tvDistance.text = getString(R.string.label_distance_none)
        tvTimeMoving.text = getString(R.string.label_time_moving_none)
        tvTimeMovingUp.text = getString(R.string.label_time_moving_up_none)
        tvTimeMovingDown.text = getString(R.string.label_time_moving_down_none)
        tvTimeStopped.text = getString(R.string.label_time_stopped_none)
        tvAltitudeHighest.text = getString(R.string.label_altitude_highest_none)
        tvAltitudeLowest.text = getString(R.string.label_altitude_lowest_none)
        tvAltitudeDifference.text = getString(R.string.label_altitude_difference_none)
    }

    companion object {
        private const val NEEDLE_SPEED: Long = 500
        private const val TIME_MILLS_TO_HOUR: Double = 3600000.0
    }
}
