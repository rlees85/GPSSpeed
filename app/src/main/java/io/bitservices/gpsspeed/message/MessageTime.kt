package io.bitservices.gpsspeed.message

data class MessageTime (
    val moving: Long,
    val movingUp: Long,
    val movingDown: Long,
    val stopped: Long
) {}