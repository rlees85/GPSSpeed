package io.bitservices.gpsspeed.message

data class MessageAltitude (
    val current: Float,
    val highest: Float,
    val lowest: Float
) {}