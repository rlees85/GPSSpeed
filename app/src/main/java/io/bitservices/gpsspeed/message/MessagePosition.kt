package io.bitservices.gpsspeed.message

data class MessagePosition (
    val total: Double,
    val up: Double,
    val down: Double
) {}