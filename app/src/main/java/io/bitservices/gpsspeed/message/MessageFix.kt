package io.bitservices.gpsspeed.message

data class MessageFix (
    val fixed: Boolean,
    val inUse: Int
) {}