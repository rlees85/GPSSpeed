package io.bitservices.gpsspeed.message

data class MessageSpeed (
    val current: Float,
    val max: Float
) {}