package io.bitservices.gpsspeed

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Intent
import android.content.SharedPreferences
import android.health.connect.datatypes.units.Power
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.location.OnNmeaMessageListener
import android.os.PowerManager
import android.os.PowerManager.PARTIAL_WAKE_LOCK
import android.os.PowerManager.WakeLock
import androidx.lifecycle.LifecycleService
import androidx.lifecycle.Observer
import io.bitservices.gpsspeed.data.Speed
import io.bitservices.gpsspeed.data.nmea.NmeaMessage
import io.bitservices.gpsspeed.data.nmea.sentence.NmeaGGA
import io.bitservices.gpsspeed.data.nmea.sentence.NmeaGNS
import io.bitservices.gpsspeed.data.nmea.sentence.NmeaGSA
import io.bitservices.gpsspeed.data.nmea.sentence.NmeaGSV
import io.bitservices.gpsspeed.data.nmea.sentence.NmeaRMC
import io.bitservices.gpsspeed.data.nmea.sentence.NmeaVTG
import io.bitservices.gpsspeed.message.MessageAltitude
import io.bitservices.gpsspeed.message.MessageFix
import io.bitservices.gpsspeed.message.MessagePosition
import io.bitservices.gpsspeed.message.MessageSpeed
import io.bitservices.gpsspeed.message.MessageTime
import io.bitservices.gpsspeed.model.SessionAltitude
import io.bitservices.gpsspeed.model.SessionFix
import io.bitservices.gpsspeed.model.SessionPosition
import io.bitservices.gpsspeed.model.SessionSatellitesView
import io.bitservices.gpsspeed.model.SessionSpeed
import io.bitservices.gpsspeed.model.SessionTime

class MainService : LifecycleService(), OnNmeaMessageListener {
    private var altitude: SessionAltitude = SessionAltitude()
    private var fix: SessionFix = SessionFix()
    private var position: SessionPosition = SessionPosition()
    private var speed: SessionSpeed = SessionSpeed()
    private var satellitesView: SessionSatellitesView = SessionSatellitesView()
    private var time: SessionTime = SessionTime()

    private lateinit var lm: LocationManager
    private val ll: LocationListener = object : LocationListener {
        override fun onLocationChanged(location: Location) {
            // We don't care.
        }

        override fun onProviderDisabled(provider: String) {
            onNmeaMessage("\$BSGPSS,0*1A", System.currentTimeMillis())
            super.onProviderDisabled(provider)
        }

        override fun onProviderEnabled(provider: String) {
            onNmeaMessage("\$BSGPSS,1*1B", System.currentTimeMillis())
            super.onProviderEnabled(provider)
        }
    }

    private val nId: Int = 1

    private lateinit var nMan: NotificationManager
    private lateinit var nBuild: Notification.Builder

    private var nmeaHasGns: Boolean = false
    private var nmeaHasRmc: Boolean = false
    private var nmeaHasVtg: Boolean = false

    private lateinit var oUi: Observer<Int>

    private lateinit var pm: PowerManager
    private lateinit var sp: SharedPreferences
    private lateinit var wl: WakeLock

    override fun onCreate() {
        super.onCreate()

        // Load saved settings
        sp = getSharedPreferences(getString(R.string.shared_prefs_trip_settings), MODE_PRIVATE)
        if (sp.getBoolean(getString(R.string.shared_prefs_trip_altitude_initialised_setting), false)) {
            altitude.restore(
                sp.getFloat(getString(R.string.shared_prefs_trip_altitude_highest_setting), 0f),
                sp.getFloat(getString(R.string.shared_prefs_trip_altitude_lowest_setting), 1f)
            )
        }

        position.restore(
            sp.getFloat(getString(R.string.shared_prefs_trip_distance_setting), 0f).toDouble(),
            sp.getFloat(getString(R.string.shared_prefs_trip_distance_up_setting), 0f).toDouble(),
            sp.getFloat(getString(R.string.shared_prefs_trip_distance_down_setting), 0f).toDouble(),
            sp.getFloat(getString(R.string.shared_prefs_trip_distance_unknown_setting), 0f).toDouble()
        )

        speed.maxSpeed = Speed.fromBoth(
            sp.getFloat(getString(R.string.shared_prefs_trip_max_speed_knots_setting), 0f),
            sp.getFloat(getString(R.string.shared_prefs_trip_max_speed_kph_setting), 0f),
        )

        time.restore(
            sp.getLong(getString(R.string.shared_prefs_trip_time_moving_setting), 0),
            sp.getLong(getString(R.string.shared_prefs_trip_time_moving_up_setting), 0),
            sp.getLong(getString(R.string.shared_prefs_trip_time_moving_down_setting), 0),
            sp.getLong(getString(R.string.shared_prefs_trip_time_moving_unknown_setting), 0),
            sp.getLong(getString(R.string.shared_prefs_trip_time_stopped_setting), 0)
        )


        // Location Manager
        lm = getSystemService(LOCATION_SERVICE) as LocationManager


        // Power Manager
        pm = getSystemService(POWER_SERVICE) as PowerManager
        wl = pm.newWakeLock(PARTIAL_WAKE_LOCK, applicationContext.packageName + "::" + getString(R.string.wakelock_id))


        // Observer (UI events)
        oUi = Observer { newState ->
            when (newState) {
                MainData.LIVEDATA_UI_CONNECTED -> {
                    // Force a UI update
                    uiUpdateAltitude(true)
                    uiUpdateFix(true)
                    uiUpdatePosition(true)
                    uiUpdateSpeed(true)
                    uiUpdateSatellitesView(true)
                    uiUpdateTime(true)
                }
                MainData.LIVEDATA_UI_RESET -> {
                    // Reset data
                    altitude = SessionAltitude()
                    position = SessionPosition()
                    speed = SessionSpeed()
                    time = SessionTime()
                    // Clear the reset flag (which should trigger a UI update)
                    MainData.ui.value = MainData.LIVEDATA_UI_CONNECTED
                }
            }
        }


        // Initialise notification
        // Setup Notification Manager
        nMan = getSystemService(NOTIFICATION_SERVICE) as NotificationManager

        // Setup Notification Channel
        val ncId: String = applicationContext.packageName + "." + applicationContext.getString(R.string.notification_channel)
        val ncName: CharSequence = applicationContext.getString(R.string.app_name)
        val ncDescription: String = applicationContext.getString(R.string.notification_description).format(applicationContext.getString(R.string.app_name))
        val nc: NotificationChannel = NotificationChannel(ncId, ncName, NotificationManager.IMPORTANCE_DEFAULT)
        nc.description = ncDescription
        nc.setShowBadge(true)
        nMan.createNotificationChannel(nc)

        // Setup Notification Intent
        val nPendingIntent: PendingIntent = PendingIntent.getActivity(applicationContext, 0, Intent(applicationContext, MainActivity::class.java), PendingIntent.FLAG_IMMUTABLE)

        // Build Notification
        nBuild = Notification.Builder(applicationContext, ncId)
            .setContentTitle(ncName)
            .setContentText(ncDescription)
            .setSmallIcon(R.drawable.ic_notification)
            .setColor(getColor(R.color.primaryDark))
            .setContentIntent(nPendingIntent)
            .setOnlyAlertOnce(true)
            .setOngoing(true)
            .setForegroundServiceBehavior(Notification.FOREGROUND_SERVICE_IMMEDIATE)

        // Start service in foreground
        startForeground(nId, nBuild.build())
    }

    @SuppressLint("MissingPermission", "WakelockTimeout")
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        // Get the wakelock
        wl.acquire()

        // Start the NMEA listener
        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0f, ll)
        lm.addNmeaListener(this, null)

        // Listen for UI events
        MainData.ui.observe(this, oUi)

        // Announce to UI the service is up
        MainData.service.postValue(true)

        // Keep the service running until it is explicitly stopped.
        super.onStartCommand(intent, flags, startId)
        return START_STICKY
    }

    override fun onDestroy() {
        // Stop subscribing to UI events
        MainData.ui.removeObserver(oUi)

        // Stop the NMEA listener
        lm.removeNmeaListener(this)
        lm.removeUpdates(ll)

        // Commit outstanding time moving/stopped
        time.add(speed.isMoving(), altitude.direction)

        // Save session state
        sp.edit()
            .putFloat(getString(R.string.shared_prefs_trip_max_speed_kph_setting), speed.maxSpeed.kph)
            .putFloat(getString(R.string.shared_prefs_trip_max_speed_knots_setting), speed.maxSpeed.knots)
            .putFloat(getString(R.string.shared_prefs_trip_distance_setting), position.distance(SessionPosition.POSITION_DISTANCE_UNIT_RAW).toFloat())
            .putFloat(getString(R.string.shared_prefs_trip_distance_up_setting), position.distanceUp(SessionPosition.POSITION_DISTANCE_UNIT_RAW).toFloat())
            .putFloat(getString(R.string.shared_prefs_trip_distance_down_setting), position.distanceDown(SessionPosition.POSITION_DISTANCE_UNIT_RAW).toFloat())
            .putFloat(getString(R.string.shared_prefs_trip_distance_unknown_setting), position.distanceUnknown().toFloat())
            .putLong(getString(R.string.shared_prefs_trip_time_moving_setting), time.timeMoving)
            .putLong(getString(R.string.shared_prefs_trip_time_moving_up_setting), time.timeMovingUp)
            .putLong(getString(R.string.shared_prefs_trip_time_moving_down_setting), time.timeMovingDown)
            .putLong(getString(R.string.shared_prefs_trip_time_moving_unknown_setting), time.timeMovingUnknown)
            .putLong(getString(R.string.shared_prefs_trip_time_stopped_setting), time.timeStopped)
            .putBoolean(getString(R.string.shared_prefs_trip_altitude_initialised_setting), altitude.initialised)
            .putFloat(getString(R.string.shared_prefs_trip_altitude_highest_setting), altitude.highest)
            .putFloat(getString(R.string.shared_prefs_trip_altitude_lowest_setting), altitude.lowest)
            .apply()

        // Wipe the messages to the UI
        MainData.serviceFix.postValue(null)
        MainData.serviceTime.postValue(null)
        MainData.serviceSpeed.postValue(null)
        MainData.serviceAltitude.postValue(null)
        MainData.servicePosition.postValue(null)
        MainData.serviceSatellitesView.postValue(null)

        // Inform the UI
        MainData.service.postValue(false)

        // Release the wakelock
        wl.release()

        // Finally
        super.onDestroy()
    }

    override fun onNmeaMessage(message: String?, timestamp: Long) {
        val nmeaMessage: NmeaMessage? = NmeaMessage(timestamp, message)

        if (nmeaMessage != null) {
            // Check for system messages
            if (nmeaMessage.system == "BS" && nmeaMessage.type == "GPSS") {
                if (nmeaMessage.getSentenceIndex(0)?.toIntOrNull() == 0) {
                    this.stopSelf()
                }

                return
            }

            // Process message
            when (nmeaMessage.type) {
                "GGA" -> {
                    val gga: NmeaGGA = NmeaGGA(nmeaMessage)

                    // Process GSA message
                    if (fix.setGGA(gga.fixType >= NmeaGGA.GGA_FIX_TYPE_SINGLE) && !fix.fixed) {
                        // Updates the fix status but also checks for "if the fix status has changed and its now lost"
                        altitude.reset()
                    }

                    if (!nmeaHasGns) {
                        // Use GNS message for satellites if possible as the
                        // GGA message is capped to only 12!
                        fix.satellites = gga.satellites
                        fix.accuracyHorizontal = gga.horizontalAccuracy
                    }
                    uiUpdateFix()

                    if(!nmeaHasGns) {
                        if (fix.isAccurateVertical()) {
                            altitude.altitude = gga.altitude.altitude
                            uiUpdateAltitude()
                        }

                        if (!nmeaHasRmc &&
                            fix.isAccurateHorizontal() &&
                            speed.isMoving()
                        ) {
                            position.add(gga.position, altitude.direction)
                            uiUpdatePosition()
                        }
                    }
                }

                "GNS" -> {
                    val gns: NmeaGNS = NmeaGNS(nmeaMessage)

                    if (!nmeaHasGns) {
                        nmeaHasGns = true
                    }

                    // Process GNS message
                    fix.satellites = gns.satellites
                    fix.accuracyHorizontal = gns.horizontalAccuracy
                    uiUpdateFix()

                    if (fix.isAccurateVertical()) {
                        altitude.altitude = gns.altitude.altitude
                        uiUpdateAltitude()
                    }

                    if(!nmeaHasRmc &&
                        fix.isAccurateHorizontal() &&
                        speed.isMoving()
                    ) {
                        position.add(gns.position, altitude.direction)
                        uiUpdatePosition()
                    }
                }

                "GSA" -> {
                    val gsa: NmeaGSA = NmeaGSA(nmeaMessage)

                    // Process GSA message
                    if (fix.setGSA(gsa.fixType >= NmeaGSA.GSA_FIX_TYPE_3D) && !fix.fixed) {
                        // Updates the fix status but also checks for "if the fix status has changed and its now lost"
                        altitude.reset()
                    }

                    fix.accuracyPositional = gsa.positionalAccuracy
                    fix.accuracyHorizontal = gsa.horizontalAccuracy
                    fix.accuracyVertical = gsa.verticalAccuracy
                    uiUpdateFix()
                }

                "GSV" -> {
                    val gsv: NmeaGSV = NmeaGSV(nmeaMessage)

                    // Process GSV message
                    satellitesView.add(gsv.getSystem(), gsv.signal, gsv.satellites)

                    // Update the UI
                    uiUpdateSatellitesView()
                }

                "RMC" -> {
                    val rmc: NmeaRMC = NmeaRMC(nmeaMessage)

                    if (!nmeaHasRmc) {
                        nmeaHasRmc = true
                    }

                    // Process RMC message
                    if (fix.isAccurateHorizontal()) {
                        if (!nmeaHasVtg) {
                            speed.speed = rmc.speed
                            uiUpdateSpeed()
                        }

                        if (speed.isMoving()) {
                            position.add(rmc.position, altitude.direction)
                            uiUpdatePosition()
                        }
                    }

                    // Update times, after speed update, regardless of accuracy.
                    if(!nmeaHasVtg) {
                        time.add(speed.isMoving(), altitude.direction)
                        uiUpdateTime()
                    }

                    // Update the UI
                }

                "VTG" -> {
                    val vtg: NmeaVTG = NmeaVTG(nmeaMessage)

                    if (!nmeaHasVtg) {
                        nmeaHasVtg = true
                    }

                    // Process VTG message
                    if (fix.isAccurateHorizontal()) {
                        speed.speed = vtg.speed
                        uiUpdateSpeed()
                    }

                    // Update times, after speed update, regardless of accuracy.
                    time.add(speed.isMoving(), altitude.direction)
                    uiUpdateTime()
                }
            }
        }
    }

    private fun uiUpdateAltitude(force: Boolean = false) {
        if (altitude.initialised &&
            MainData.serviceAltitude.hasObservers() &&
            (force || altitude.shouldSend())) {
            MainData.serviceAltitude.postValue(MessageAltitude(altitude.altitude, altitude.highest, altitude.lowest))
        }
    }

    private fun uiUpdateFix(force: Boolean = false) {
        if (MainData.serviceFix.hasObservers() &&
            (force || fix.shouldSend())) {
            MainData.serviceFix.postValue(MessageFix(fix.fixed, fix.satellites))
        }
    }

    private fun uiUpdatePosition(force: Boolean = false) {
        if (MainData.servicePosition.hasObservers() &&
            (force || position.shouldSend())) {
            MainData.servicePosition.postValue(MessagePosition(
                position.distance(SessionPosition.POSITION_DISTANCE_UNIT_MILES),
                position.distanceUp(SessionPosition.POSITION_DISTANCE_UNIT_MILES),
                position.distanceDown(SessionPosition.POSITION_DISTANCE_UNIT_MILES)))
        }
    }

    private fun uiUpdateSpeed(force: Boolean = false) {
        if (MainData.serviceSpeed.hasObservers() &&
            (force || speed.shouldSend())) {
            MainData.serviceSpeed.postValue(MessageSpeed(speed.speed.mph, speed.maxSpeed.mph))
        }
    }

    private fun uiUpdateSatellitesView(force: Boolean = false) {
        if (MainData.serviceFix.hasObservers() &&
            (force || satellitesView.shouldSend())) {
            MainData.serviceSatellitesView.postValue(satellitesView.toString())
        }
    }

    private fun uiUpdateTime(force: Boolean = false) {
        if (MainData.serviceTime.hasObservers() &&
            (force || time.shouldSend())) {
            MainData.serviceTime.postValue(MessageTime(time.timeMoving, time.timeMovingUp, time.timeMovingDown, time.timeStopped))
        }
    }
}