package io.bitservices.gpsspeed.data

class Speed private constructor(val knots: Float, val kph: Float, val mph: Float){
    companion object {
        private const val KNOTS_TO_KPH: Float = 1.8519993f
        private const val KNOTS_TO_MPH: Float = 1.1507795f
        private const val KPH_TO_MPH: Float = 0.621371f
        private const val KPH_TO_KNOTS: Float = 0.539957f

        fun fromBoth(knots: Float, kph: Float): Speed {
            val mph: Float = kph * KPH_TO_MPH
            return Speed(knots, kph, mph)
        }

        fun fromKnots(knots: Float): Speed {
            val kph: Float = knots * KNOTS_TO_KPH
            val mph: Float = knots * KNOTS_TO_MPH
            return Speed(knots, kph, mph)
        }

        fun fromKph(kph: Float): Speed {
            val knots: Float = kph * KPH_TO_KNOTS
            val mph: Float = kph * KPH_TO_MPH
            return Speed(knots, kph, mph)
        }

        fun fromNone(): Speed {
            return Speed(0f, 0f, 0f)
        }
    }
}