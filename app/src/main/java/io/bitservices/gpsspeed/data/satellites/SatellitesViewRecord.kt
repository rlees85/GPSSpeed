package io.bitservices.gpsspeed.data.satellites

import java.time.Instant

data class SatellitesViewRecord (
    val satellites: Int,
    val timestamp: Instant = Instant.now()
) {}