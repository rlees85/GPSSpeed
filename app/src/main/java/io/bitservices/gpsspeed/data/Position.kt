package io.bitservices.gpsspeed.data

import kotlin.math.floor
import kotlin.math.pow

class Position(val latitude: Double = 0.0, val longitude: Double = 0.0, val valid: Boolean = false){
    companion object {
        private const val LATITUDE_DIRECTION_NORTH: String = "N"
        private const val LATITUDE_DIRECTION_SOUTH: String = "S"
        private const val LONGITUDE_DIRECTION_EAST: String = "E"
        private const val LONGITUDE_DIRECTION_WEST: String = "W"

        fun fromNmea(latitudeNmea: Double,
                     latitudeDirection: String,
                     longitudeNmea: Double,
                     longitudeDirection: String): Position {

            // Check validity
            if (!arrayOf(LATITUDE_DIRECTION_NORTH, LATITUDE_DIRECTION_SOUTH).contains(latitudeDirection)) {
                throw Exception("Latitude direction: $latitudeDirection is not valid!")
            }

            if (!arrayOf(LONGITUDE_DIRECTION_EAST, LONGITUDE_DIRECTION_WEST).contains(longitudeDirection)) {
                throw Exception("Longitude direction: $longitudeDirection is not valid!")
            }

            // Do it.
            return Position(nmeaToDec(latitudeNmea, latitudeDirection),
                            nmeaToDec(longitudeNmea, longitudeDirection),true)
        }

        private fun nmeaToDec(nmea: Double, direction: String): Double {
            val degrees: Double = floor(nmea / 100.0)
            val decimal: Double = doubleRound(degrees + ((nmea - (degrees * 100.0)) / 60.0))
            return if (listOf(LATITUDE_DIRECTION_SOUTH, LATITUDE_DIRECTION_NORTH).contains(direction)) {
                decimal * -1.0
            } else {
                decimal
            }
        }

        private fun doubleRound(double: Double): Double {
            val places: Double = 5.0
            val i: Int = (double * (10.0.pow(places))).toInt()
            return i.toDouble() / (10.0.pow(places))
        }
    }
}