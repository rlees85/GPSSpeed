package io.bitservices.gpsspeed.data.nmea.sentence

import io.bitservices.gpsspeed.data.Altitude
import io.bitservices.gpsspeed.data.Position
import io.bitservices.gpsspeed.data.nmea.NmeaMessage
import io.bitservices.gpsspeed.data.nmea.NmeaSentence

class NmeaGNS(private val message: NmeaMessage) : NmeaSentence(message) {
    val altitude: Altitude
    val horizontalAccuracy: Float
    val position: Position
    val satellites: Int // In use

    init {
        // Parse position
        var position: Position = Position()
        val latitudeNmea: Double = message.getSentenceIndex(1)?.toDoubleOrNull() ?: 0.0
        val latitudeDirection: String? = message.getSentenceIndex(2)?.trim()?.uppercase()
        val longitudeNmea: Double = message.getSentenceIndex(3)?.toDoubleOrNull() ?: 0.0
        val longitudeDirection: String? = message.getSentenceIndex(4)?.trim()?.uppercase()

        if (latitudeDirection != null && longitudeDirection != null) {
            position = try {
                Position.fromNmea(latitudeNmea, latitudeDirection, longitudeNmea, longitudeDirection)
            } catch (err: Exception) {
                Position()
            }
        }
        this.position = position

        // Satellites in use
        satellites = message.getSentenceIndex(6)?.toIntOrNull() ?: 0

        // Process accuracy information
        horizontalAccuracy = message.getSentenceIndex(7)?.toFloatOrNull() ?: Float.MAX_VALUE

        // Altitude
        val altitudeMetersRaw: Float? = message.getSentenceIndex(8)?.toFloatOrNull()
        val undulationMetersRaw: Float? = message.getSentenceIndex(10)?.toFloatOrNull()
        altitude = Altitude(altitudeMetersRaw ?: 0f, undulationMetersRaw ?: 0f)
    }
}