package io.bitservices.gpsspeed.data.nmea

import java.time.ZonedDateTime

open class NmeaSentence(private val message: NmeaMessage) {
    fun getSystem(): String {
        return message.system
    }

    fun getTimestamp(): ZonedDateTime {
        return message.timestamp
    }
}