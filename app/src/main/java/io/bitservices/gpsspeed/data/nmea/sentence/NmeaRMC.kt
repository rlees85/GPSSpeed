package io.bitservices.gpsspeed.data.nmea.sentence

import io.bitservices.gpsspeed.data.Position
import io.bitservices.gpsspeed.data.Speed
import io.bitservices.gpsspeed.data.nmea.NmeaMessage
import io.bitservices.gpsspeed.data.nmea.NmeaSentence

class NmeaRMC(private val message: NmeaMessage) : NmeaSentence(message) {
    val position: Position
    val speed: Speed
    val valid: Boolean // Must be checked before using any other properties!

    init {
        // Get validity
        valid = message.getSentenceIndex(1)?.trim()?.uppercase()?.equals("A") ?: false

        // Parse speed
        val knotsRaw: Float? = message.getSentenceIndex(6)?.toFloatOrNull()
        speed = if (knotsRaw != null) Speed.fromKnots(knotsRaw) else Speed.fromNone()

        // Parse position
        var position: Position = Position()
        val latitudeNmea: Double = message.getSentenceIndex(2)?.toDoubleOrNull() ?: 0.0
        val latitudeDirection: String? = message.getSentenceIndex(3)?.trim()?.uppercase()
        val longitudeNmea: Double = message.getSentenceIndex(4)?.toDoubleOrNull() ?: 0.0
        val longitudeDirection: String? = message.getSentenceIndex(5)?.trim()?.uppercase()

        if (latitudeDirection != null && longitudeDirection != null) {
            position = try {
                Position.fromNmea(latitudeNmea, latitudeDirection, longitudeNmea, longitudeDirection)
            } catch (err: Exception) {
                Position()
            }
        }
        this.position = position
    }
}