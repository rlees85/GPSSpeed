package io.bitservices.gpsspeed.data.nmea.sentence

import io.bitservices.gpsspeed.data.nmea.NmeaMessage
import io.bitservices.gpsspeed.data.nmea.NmeaSentence

class NmeaGSA(private val message: NmeaMessage) : NmeaSentence(message) {

    val fixType: Int

    val positionalAccuracy: Float
    val horizontalAccuracy: Float
    val verticalAccuracy: Float

    init {
        // Process fix information
        var fixTypeResult: Int = GSA_FIX_TYPE_UNKNOWN
        val fixTypeRaw: Int? = message.getSentenceIndex(1)?.toIntOrNull()

        if (fixTypeRaw != null &&
            fixTypeRaw >= 1 &&
            fixTypeRaw <= 3) {
            fixTypeResult = fixTypeRaw
        }
        fixType = fixTypeResult

        // Process accuracy information
        positionalAccuracy = message.getSentenceIndex(14)?.toFloatOrNull() ?: Float.MAX_VALUE
        horizontalAccuracy = message.getSentenceIndex(15)?.toFloatOrNull() ?: Float.MAX_VALUE
        verticalAccuracy = message.getSentenceIndex(16)?.toFloatOrNull() ?: Float.MAX_VALUE
    }

    companion object {
        const val GSA_FIX_TYPE_UNKNOWN: Int = 0
        const val GSA_FIX_TYPE_NONE: Int = 1
        const val GSA_FIX_TYPE_2D: Int = 2
        const val GSA_FIX_TYPE_3D: Int = 3
    }

}