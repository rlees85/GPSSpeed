package io.bitservices.gpsspeed.data.nmea.sentence

import io.bitservices.gpsspeed.data.Speed
import io.bitservices.gpsspeed.data.nmea.NmeaMessage
import io.bitservices.gpsspeed.data.nmea.NmeaSentence

class NmeaVTG(private val message: NmeaMessage) : NmeaSentence(message) {
    val speed: Speed

    init {
        // Process speed information
        val knotsCheck: Boolean = message.getSentenceIndex(5)?.trim()?.uppercase()?.equals(VTG_UNIT_KNOTS) ?: false
        val kphCheck: Boolean = message.getSentenceIndex(7)?.trim()?.uppercase()?.equals(VTG_UNIT_KPH) ?: false

        val knotsRaw: Float? = message.getSentenceIndex(4)?.toFloatOrNull()
        val kphRaw: Float? = message.getSentenceIndex(6)?.toFloatOrNull()

        val hasKnots: Boolean = knotsCheck && knotsRaw != null
        val hasKph: Boolean = kphCheck && kphRaw != null

        if (hasKnots && hasKph) {
            // Has a reading in both KPH and KNOTS
            speed = Speed.fromBoth(knotsRaw ?: 0f, kphRaw ?: 0f)
        } else if (hasKnots) {
            // Only has reading in KNOTS
            speed = Speed.fromKnots(knotsRaw ?: 0f)
        } else if (hasKph) {
            // Only has reading in KPH
            speed = Speed.fromKph(kphRaw ?: 0f)
        } else {
            // Has no readings
            speed = Speed.fromNone()
        }
    }

    companion object {
        const val VTG_UNIT_KNOTS: String = "N"
        const val VTG_UNIT_KPH: String = "K"
    }
}