package io.bitservices.gpsspeed.data.nmea

import java.time.Instant
import java.time.ZoneId
import java.time.ZonedDateTime

class NmeaMessage private constructor(val timestamp: ZonedDateTime, val system: String, val type: String, private val sentence: List<String>) {
    fun getSentenceIndex(index: Int): String? {
        return if (sentence.indices.contains(index) && sentence[index] != "") {
            sentence[index]
        } else {
            null
        }
    }

    fun getSentenceFirst(): String? {
        return if (sentence.first() != "") {
            sentence.first()
        } else {
            null
        }
    }

    fun getSentenceLast(): String? {
        return if (sentence.last() != "") {
            sentence.last()
        } else {
            null
        }
    }

    companion object {
        private const val NMEA_TIME_ZONE = "UTC"

        operator fun invoke(timestamp: Long, nmea: String?): NmeaMessage? {
            // If nmea is null, then we can only return null
            if (nmea == null) {
                return null
            }

            // Strip and verify the NMEA string.
            val verifiedStrippedNmea: String = stripAndVerify(nmea) ?: return null
            val verifiedStrippedNmeaParts: List<String> = verifiedStrippedNmea.split(",")

            // If the stripped NMEA string has one (or less) components it cannot be valid
            if (verifiedStrippedNmeaParts.count() < 2) {
                return null
            }

            // Get the nmea key - $<SYSTEM><TYPE> where <SYSTEM> could be GP (for GPS) and
            // type could be RMC (for recommended minimum specific GPS data).
            val nmeaKey: String = getType(verifiedStrippedNmeaParts[0]) ?: return null

            if (nmeaKey.length < 3) {
                return null
            }

            val nmeaSystem: String = nmeaKey.take(2)
            val nmeaType: String = nmeaKey.removePrefix(nmeaSystem)

            // Return an instance of this class
            return NmeaMessage(ZonedDateTime.ofInstant(Instant.ofEpochMilli(timestamp), ZoneId.of(NMEA_TIME_ZONE)), nmeaSystem, nmeaType, verifiedStrippedNmeaParts.drop(1))
        }

        private fun stripAndVerify(nmea: String): String? {
            val nmeaBits: List<String> = nmea.split("*", limit = 2)

            if (nmeaBits.count() != 2) {
                return null
            }

            val nmeaStripped: String = nmeaBits[0]
            val nmeaSum: String = nmeaBits[1].trim().uppercase()
            val nmeaExpectedSum: String = generateChecksum(nmeaStripped)

            return if (nmeaSum == nmeaExpectedSum) {
                nmeaStripped
            } else {
                null
            }
        }

        private fun generateChecksum(strippedNmea: String): String {
            var checksum = 0
            for (character in strippedNmea.replace("$", "").toCharArray()) {
                when (character) {
                    '*' -> break
                    else -> checksum = checksum xor character.code
                }
            }

            return String.format("%02X", checksum).trim().uppercase()
        }

        private fun getType(nmeaType: String): String? {
            return if (nmeaType.startsWith("$") && nmeaType.length > 1) {
                nmeaType.removePrefix("$").trim().uppercase()
            } else {
                null
            }
        }
    }
}