package io.bitservices.gpsspeed.data.nmea.sentence

import io.bitservices.gpsspeed.data.Altitude
import io.bitservices.gpsspeed.data.Position
import io.bitservices.gpsspeed.data.nmea.NmeaMessage
import io.bitservices.gpsspeed.data.nmea.NmeaSentence

class NmeaGGA(private val message: NmeaMessage) : NmeaSentence(message) {
    val altitude: Altitude
    val fixType: Int
    val horizontalAccuracy: Float
    val position: Position
    val satellites: Int // In use

    init {
        // Process fix information
        var fixTypeResult: Int = GGA_FIX_TYPE_UNKNOWN
        val fixTypeRaw: Int? = message.getSentenceIndex(5)?.toIntOrNull()

        if (fixTypeRaw != null &&
            fixTypeRaw >= 0 &&
            fixTypeRaw <= 9) {
            fixTypeResult = fixTypeRaw
        }
        fixType = fixTypeResult

        // Parse position
        var position: Position = Position()
        val latitudeNmea: Double = message.getSentenceIndex(1)?.toDoubleOrNull() ?: 0.0
        val latitudeDirection: String? = message.getSentenceIndex(2)?.trim()?.uppercase()
        val longitudeNmea: Double = message.getSentenceIndex(3)?.toDoubleOrNull() ?: 0.0
        val longitudeDirection: String? = message.getSentenceIndex(4)?.trim()?.uppercase()

        if (latitudeDirection != null && longitudeDirection != null) {
            position = try {
                Position.fromNmea(latitudeNmea, latitudeDirection, longitudeNmea, longitudeDirection)
            } catch (err: Exception) {
                Position()
            }
        }
        this.position = position

        // Satellites in use
        satellites = message.getSentenceIndex(6)?.toIntOrNull() ?: 0

        // Process accuracy information
        horizontalAccuracy = message.getSentenceIndex(7)?.toFloatOrNull() ?: Float.MAX_VALUE

        // Altitude
        val altitudeMetersCheck: Boolean = message.getSentenceIndex(9)?.trim()?.uppercase()?.equals(NmeaGGA.GGA_UNIT_METERS) ?: false
        val undulationMetersCheck: Boolean = message.getSentenceIndex(11)?.trim()?.uppercase()?.equals(NmeaGGA.GGA_UNIT_METERS) ?: false
        val altitudeMetersRaw: Float? = if (altitudeMetersCheck) message.getSentenceIndex(8)?.toFloatOrNull() else null
        val undulationMetersRaw: Float? = if(undulationMetersCheck) message.getSentenceIndex(10)?.toFloatOrNull() else null
        altitude = Altitude(altitudeMetersRaw ?: 0f, undulationMetersRaw ?: 0f)
    }

    companion object {
        const val GGA_FIX_TYPE_UNKNOWN: Int = -1
        const val GGA_FIX_TYPE_NONE: Int = 0
        const val GGA_FIX_TYPE_SINGLE: Int = 1
        const val GGA_FIX_TYPE_PSEUDORANGE: Int = 2
        const val GGA_FIX_TYPE_RTK_FIXED: Int = 4
        const val GGA_FIX_TYPE_RTK_FLOATING: Int = 5
        const val GGA_FIX_TYPE_DEAD_RECKONING: Int = 6
        const val GGA_FIX_TYPE_MANUAL: Int = 7
        const val GGA_FIX_TYPE_SIMULATOR: Int = 8
        const val GGA_FIX_TYPE_WAAS: Int = 9
        const val GGA_UNIT_METERS: String = "M"
    }
}