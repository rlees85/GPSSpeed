package io.bitservices.gpsspeed.data

class Altitude constructor(val altitude: Float, val undulation: Float = 0f){}