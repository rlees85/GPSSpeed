package io.bitservices.gpsspeed.data.nmea.sentence

import io.bitservices.gpsspeed.data.nmea.NmeaMessage
import io.bitservices.gpsspeed.data.nmea.NmeaSentence

class NmeaGSV(private val message: NmeaMessage) : NmeaSentence(message) {
    // Process fix information
    val satellites: Int = message.getSentenceIndex(2)?.toIntOrNull() ?: 0 // In view
    val signal: String = message.getSentenceLast() ?: SIGNAL_UNKNOWN

    companion object {
        const val SIGNAL_UNKNOWN: String = "U"
    }
}