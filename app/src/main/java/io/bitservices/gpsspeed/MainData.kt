package io.bitservices.gpsspeed

import androidx.lifecycle.MutableLiveData
import io.bitservices.gpsspeed.message.MessageAltitude
import io.bitservices.gpsspeed.message.MessageFix
import io.bitservices.gpsspeed.message.MessagePosition
import io.bitservices.gpsspeed.message.MessageSpeed
import io.bitservices.gpsspeed.message.MessageTime

class MainData {
    companion object {
        const val LIVEDATA_UI_DISCONNECTED: Int = 0
        const val LIVEDATA_UI_CONNECTED: Int = 1
        const val LIVEDATA_UI_RESET: Int = 2

        val service: MutableLiveData<Boolean> by lazy {
            MutableLiveData<Boolean>()
        }

        val serviceAltitude: MutableLiveData<MessageAltitude?> by lazy {
            MutableLiveData<MessageAltitude?>()
        }

        val serviceFix: MutableLiveData<MessageFix?> by lazy {
            MutableLiveData<MessageFix?>()
        }

        val servicePosition: MutableLiveData<MessagePosition?> by lazy {
            MutableLiveData<MessagePosition?>()
        }

        val serviceSatellitesView: MutableLiveData<String?> by lazy {
            MutableLiveData<String?>()
        }

        val serviceSpeed: MutableLiveData<MessageSpeed?> by lazy {
            MutableLiveData<MessageSpeed?>()
        }

        val serviceTime: MutableLiveData<MessageTime?> by lazy {
            MutableLiveData<MessageTime?>()
        }

        val ui: MutableLiveData<Int> by lazy {
            MutableLiveData<Int>()
        }
    }
}